import numpy as np

np.set_printoptions(precision=4, 
                    formatter={'float': '{: 0.2f}'.format})    # 0.125 >> 0.13

X = np.array([[1.0, 3.0], [1.0, 2.0], [2.0, 3.0], [0.0, 3.0], 
              [5.0, 4.0], [4.0, 5.0], [5.0, 5.0], [3.0, 4.0]]) # 2x8
print(X)
Xt = X.transpose()  # 2x8 >> 8x2
print(Xt)           # 8x2

mu = X.mean(axis=0)
print(mu)           # 1x2

D = X - mu
print(D)            # 8x2
Dt = D.transpose()  # 2x8
print(Dt)
