############
Plan
############

#. This is a test from Eclipse IDE...

#. Code: C

    .. literalinclude:: ../../src/Hello.c
        :linenos:

#. Code: C++

    .. literalinclude:: ../../src/Hello.cpp
        :linenos:

#. Code: Java

    .. literalinclude:: ../../src/Hello.java
        :linenos:

#. Code: Python

    .. literalinclude:: ../../src/Hello.py
        :linenos:

#. Code: HTML

    .. literalinclude:: ../../src/Hello.html
        :linenos:

#. Code: PlantUML

    .. uml:: Seq.puml
        :align: center

#. Code: Graphviz

    .. uml:: Graphviz.puml
        :align: center
