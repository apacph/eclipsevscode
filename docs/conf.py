# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sys
import os

############################################################
# Global Variables
############################################################
# -- Project information -----------------------------------------------------
version = 'v20230330'
project = u'Lab: EclipseVSCode'
copyright = u'2000-2024, Po-Hsun Cheng, Ph.D.'
author = 'Po-Hsun Cheng, Ph.D.'
release = version

templates_path = ['_templates']
source_suffix = '.rst'
source_encoding = 'utf-8-sig'
master_doc = 'index'
language = 'en'         # 'zh_TW'
#today_fmt = '%B %d, %Y'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
#default_role = None
#add_function_parentheses = True
#add_module_names = True
#show_authors = False
pygments_style = 'sphinx'
#modindex_common_prefix = []
#keep_warnings = False
todo_include_todos = False

# -- General configuration ---------------------------------------------------
extensions = []
extensions.append('sphinx.ext.autosectionlabel')    # Reference link
extensions.append('sphinx_copybutton')              # Not workable in sphinx-immaterial theme
extensions.append('sphinx_autorun')                 # ?
extensions.append('sphinx.ext.todo')                # TODO

#-----------------------------------------------------
#-- Minor Extension
extensions.append('sphinxcontrib.httpdomain')  # HTTP API
extensions.append('sphinx.ext.ifconfig')       # Network: status

extensions.append('sphinx.ext.autodoc')        # DOC: include, make html?
extensions.append('sphinx.ext.coverage')       # DOC: coverage stats
extensions.append('sphinx.ext.intersphinx')    # URL: Link
extensions.append('sphinx.ext.viewcode')       # URL: Link Code

#extensions.append('sphinxcontrib.blockdiag')   # Diagram: block
#extensions.append('sphinxcontrib.seqdiag')     # Diagram: sequence
#extensions.append('sphinxcontrib.nwdiag')      # Diagram: network
#extensions.append('sphinxcontrib.actdiag')     # Diagram: activity

#extensions.append('matplotlib.sphinxext.plot_directive')

############################################################
# Graphviz
# Note:
#   1. If it is not workable, check you install [Graphviz.msi] in [D:\Graphviz\bin]
#   2. Remember set environment: 
#           GRAPHVIZ_DOT = D:\Graphviz\bin\dot.exe
# Ref: https://jbms.github.io/sphinx-immaterial/graphviz.htm 
############################################################
extensions.append('sphinx.ext.graphviz')            # Graphviz
extensions.append("sphinx_immaterial.graphviz")     # Graphviz: sphinx_immaterial
graphviz_ignore_incorrect_font_metrics = True

############################################################
# Tabs: helveg-sphinx-code-tabs: sphinx_code_tabs
# Note: Still wait for upgrading version to support Sphinx > v5.3.0
############################################################
extensions.append('sphinx_code_tabs')               # Tabs

############################################################
# Tabs: sphinx_tabs.tabs [x]
############################################################
#extensions.append('sphinx_tabs.tabs')               # Tabs
#sphinx_tabs_valid_builders = ['linkcheck']
#sphinx_tabs_disable_tab_closing = True
#sphinx_tabs_disable_css_loading = True

############################################################
# Bibliography: sphinxcontrib.bibtex
############################################################
extensions.append('sphinxcontrib.bibtex')   # bibliography

bibtex_bibfiles = [
    'Bibliography/refJournal-20220508-cph.bib',
    'Bibliography/refJournalUnpublished-20220508-cph.bib',
    'Bibliography/refBookChapter-20220508-cph.bib',
    'Bibliography/refConference-20220508-cph.bib',
]


############################################################
# PlantUML
# Note: Ubuntu v22.04 ONLY supports plantuml v1.2020.2
#       It is NOT necessary to manually upload latest plantuml.jar.
#       Hence, we use docker image [ub22-py311-sp6-puml202]
############################################################
extensions.append('sphinxcontrib.plantuml') # plantuml

#=================================
# Gitlab Docker Usage
#=================================
# DO NOTHING HERE

#=================================
# DOS Usage: Create docs/build/html etc.
#=================================
#-- We use static path to show the diagram;
#-- However, we MUST remember to adjust it manually.
plantuml = 'java -jar /usr/share/plantuml/plantuml.jar -charset UTF-8 '
#plantuml = 'java -jar D:\\plantuml.jar '

#-- We have to refactor the following two lines back.
#plantjar = os.getenv('PLANTUML')
#plantuml = 'java -jar %s ' % plantjar

############################################################
# Mathjax
############################################################
extensions.append('sphinx.ext.mathjax')     # toggle with sphinxcontrib-katex
mathjax3_config = {
    'chtml': {
#       'displayAlign': 'left', 
#       'displayIndent': '1em',
        }, 
    'tex': {
        'tags': 'ams',
        'useLabelIds': True,
        },
    }

#-- Sphinx v3 + MathJax v3
mathjax_path = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"

'''
#-- Sphinx v2 or earlier
#mathjax_path = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
mathjax_config = {
    'extensions': ['tex2jax.js'],
    'jax': ['input/TeX', 'output/HTML-CSS'],
}
'''

math_number_all = True
math_eqref_format = "Eq. {number}"
math_numfig = True
numfig = True
numfig_secnum_depth = 8

############################################################
# Katex
############################################################
#extensions.append('sphinxcontrib-katex')   # toggle with sphinx.ext.mathjax

katex_css_path = 'https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css'
katex_js_path = 'https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js'
katex_autorender_path = 'https://cdn.jsdelivr.net/npm/katex@0.11.1/contrib/auto-render.min.js'
katex_inline = [r'\(', r'\)']
katex_display = [r'\[', r'\]']
katex_prerender = True

'''
#katex_options = ''

katex_options = r{
    displayMode: true,
    macros: {
        "\\RR": "\\mathbb{R}"
    }
}
'''

'''
#--- LaTeX Macros
#katex_options = macros: {
#        "\\i": "\\mathrm{i}",
#        "\\e": "\\mathrm{e}^{#1}",
#        "\\vec": "\\mathbf{#1}",
#        "\\x": "\\vec{x}",
#        "\\d": "\\operatorname{d}\\!{}",
#        "\\dirac": "\\operatorname{\\delta}\\left(#1\\right)",
#        "\\scalarprod": "\\left\\langle#1,#2\\right\\rangle",
#    }

'''


############################################################
# sphinx_exec_code [o][x]
############################################################
extensions.append('sphinx_execute_code')

exec_code_working_dir = '.'
exec_code_folders = ['.']
exec_code_example_dir = '.'
exec_code_set_utf8_encoding = True


############################################################
# btd.sphinx.inheritance-diagram [o][x]
############################################################
extensions.append('sphinx.ext.inheritance_diagram') # Diagram

############################################################
# Fontpath for blockdiag (truetype font)
############################################################
blockdiag_fontpath = '/usr/share/fonts/TakaoFonts/TakaoPGothic.ttf'

############################################################
# Options for HTML output
#***********************************************************
# Ref:
#    GitlabRepo: https://gitlab.com/tsgkdt/sphinx-plantuml
#    DockerHub: https://hub.docker.com/r/tsgkadot/sphinx-plantuml
############################################################
#-----------------------------------------------------------
# Theme: sphinx_immaterial
#-----------------------------------------------------------
extensions.append('sphinx_immaterial')
html_theme = 'sphinx_immaterial'
html_theme_options = {
    "version_dropdown": True,
    "version_info": [
        {"version": "v20230319", "title": "v20230319", "aliases": ["latest"]},
        {"version": "v20230318", "title": "v20230318", "aliases": []},
        {"version": "v20221127", "title": "v20221127", "aliases": []},
        {"version": "v20220527", "title": "v20220527", "aliases": []},
        {"version": "v20220411", "title": "v20220411", "aliases": []},
        {"version": "v20160101", "title": "v20160101", "aliases": []},
    ],
    # Set the name of the project to appear in the navigation.
    'nav_title': 'Lab: EclipseVSCode',

    # Set you GA account ID to enable tracking
    'google_analytics_account': 'UA-XXXXX',

    # Specify a base_url used to generate sitemap.xml. If not
    # specified, then no sitemap will be built.
    'base_url': 'https://apacph.github.io/sphinx',

    # Set the color and the accent color
    'color_primary': 'blue',
    'color_accent': 'light-blue',

    # Set the repo location to get a badge with stats
    'repo_url': 'https://github.com/apacph/sphinx/',
    'repo_name': 'Lab: EclipseVSCode',

    # Visible levels of the global TOC; -1 means unlimited
    'globaltoc_depth': 3,
    # If False, expand all TOC entries
    'globaltoc_collapse': False,
    # If True, show hidden TOC entries
    'globaltoc_includehidden': False,
    
    # Google analytics
    "analytics": {
        "provider": "google",
        "property": "G-XXXXXXXXXX",
        "feedback": {
            "title": "Was this page helpful?",
            "ratings": [
                {
                    "icon": "material/emoticon-happy-outline",
                    "name": "This page was helpful",
                    "data": 1,
                    "note": "Thanks for the feedback!",
                },
                {
                    "icon": "material/emoticon-sad-outline",
                    "name": "This page could be improved",
                    "data": 0,
                    "note": "Thanks for the feedback! Help us improve this page by "
                    '<a href="https://github.com/jbms/sphinx-immaterial/issues">opening an issue</a>.',
                },
            ],
        },
    },
}

templates_path = ["_templates"]

#-----------------------------------------------------------
# Theme: Sphinxbootstrap4theme
#-----------------------------------------------------------
'''
import sphinxbootstrap4theme

html_theme = 'sphinxbootstrap4theme'
#html_theme = 'epub'
#html_theme = 'alabaster'

html_theme_path = [sphinxbootstrap4theme.get_path()]

html_logo = '_static/Logo-WhoNotes.png'
#html_favicon = '_static/Logo-WhoNotes.png'
html_title =  '<project> v<revision> Documentation'
#html_title =  project
#html_short_title = None
html_output_encoding = 'utf-8'
'''
#------------------------------
# HTML Theme Option @ Gitlab
#------------------------------
html_theme_options = {
    'navbar_links': [
         ('Home', 'index', False),
         ("About", "https://apacph.gitlab.io/about", True ),
    ]
}

#------------------------------
# Path
#------------------------------
html_baseurl = ''
html_static_path = ['_static']
#html_extra_path = []
#html_sidebars = {}
#html_additional_pages = {}

#------------------------------
# Single HTML output
#------------------------------
#singlehtml_sidebars = 
'''
html_sidebars = {
   '**': ['globaltoc.html', 'sourcelink.html', 'searchbox.html'],
   'using/windows': ['windowssidebar.html', 'searchbox.html'],
}
'''

#------------------------------
# Help output
#------------------------------
htmlhelp_basename = 'Python'
#htmlhelp_basename = 'pydoc'
#htmlhelp_file_suffix = '.html'
#htmlhelp_link_suffix = '.html'

#------------------------------
# Math
#------------------------------
html_math_renderer = 'mathjax'

#------------------------------
# Format
#------------------------------
#html_style = 
#html_codeblock_linenos_style = 'table'
#html_codeblock_linenos_style = 'inline' 
#html_context = 
#html_last_updated_fmt = None
html_use_smartypants = True

#------------------------------
# Links
#------------------------------
#html_add_permalinks =            # Deprecated
#html_permalinks = True
#html_scaled_image_link = 

#------------------------------
# Suffix
#------------------------------
html_secnumber_suffix = '. '
#html_file_suffix = '.html'
#html_file_suffix = None
#html_link_suffix = 
#html_sourcelink_suffix = '.txt'

#------------------------------
# Index
#------------------------------
#html_domain_indices = True
html_use_index = False
#html_split_index = False

#------------------------------
# Search
#------------------------------
html_use_opensearch = ''
#html_search_language = 'en'
#html_search_options = {'type': 'default'}
#html_search_scorer = 
#html_show_search_summary = True

'''
#html_search_options = {'type': 'default'}

html_search_options = {
    'type': 'mecab',
    'dic_enc': 'utf-8',
    'dict': '/path/to/mecab.dic',
    'lib': '/path/to/libmecab.so',
}
'''

#------------------------------
# Copy
#------------------------------
html_copy_source = True

#------------------------------
# Show
#------------------------------
html_show_sourcelink = False
html_show_sphinx = True
html_show_copyright = True

#------------------------------
# Others
#------------------------------
#html_compact_lists = True
#html_experimental_html5_writer = False
#html4_writer = False
'''
html_additional_pages = {
    'download': 'customdownload.html',
}

html_css_files = ['custom.css',
                  'https://example.com/css/custom.css',
                  ('print.css', {'media': 'print'})]

html_js_files = ['script.js',
                 'https://example.com/scripts/custom.js',
                 ('custom.js', {'async': 'async'})]
'''

############################################################
# Options for Epub output
############################################################
epub_basename = u'Lab: EclipseVSCode'
epub_author = u'Po-Hsun Cheng, Ph.D.'
epub_publisher = u'Po-Hsun Cheng, Ph.D.'
epub_scheme = 'UUID'
epub_identifier = '88810006-1217-0726-5626-cd39078170d6'
epub_uid = "urn:uuid:88810006-1217-0726-5626-cd39078170d6"
#epub_cover = ('', '_static/Logo-WhoNotes.png')
epub_tocdepth = 5
epub_tocdup = False
epub_exclude_files = [ 
    '_static/Sphinx/aopsmods.js', 
    '_static/Sphinx/basic.css', 
    '_static/Sphinx/codemirrorEdited.css', 
    '_static/Sphinx/doctools.js', 
    '_static/Sphinx/jquery.js', 
    '_static/Sphinx/pygments.css', 
    '_static/Sphinx/pywindowCodemirrorC.js', 
    '_static/Sphinx/skulpt.min.js', 
    '_static/Sphinx/skulpt-stdlib.js', 
    '_static/Sphinx/style.css', 
    '_static/Sphinx/underscore.js', 
    'search.html', 
    ]

############################################################
# Options for LaTeX output
############################################################
latex_engine = 'pdflatex'

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
'preamble': '''
\\hypersetup{unicode=true}
\\usepackage{CJKutf8}
\\AtBeginDocument{\\begin{CJK}{UTF8}{gbsn}}
\\AtEndDocument{\\end{CJK}}
''',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'main.tex', 'Lab: EclipseVSCode',
     'Po-Hsun Cheng, Ph.D.', 'report')
]

latex_logo = 'W:/_D/Data.cph/Workspace/Py3/Py7.Lab.Template/docs/_static/myTemplate.png'
latex_show_urls = 'footnote'


############################################################
# Global Variables Definition Area 
############################################################
ultimate_replacements = {
    "{pCalculatorExeWeb}"     : "W:/_D/Data.cph/Workspace/Python3/Py2.Exe.Pyinstaller.Calculator/docs/build/html",

}


############################################################
# Function: Global Variables Definition Area
# Author:   CPH
# Start:    20190207
############################################################
def ultimateReplace(app, docname, source):
    result = source[0]
    for key in app.config.ultimate_replacements:
        result = result.replace(key, app.config.ultimate_replacements[key])
    source[0] = result

def setup(app):
    # For equation usage
    #---------------------------------------------
    # sphinx-material Theme: 
    #---------------------------------------------
    app.add_css_file('_static/custom.css')
    
    
    app.add_config_value('ultimate_replacements', {}, True)
    app.connect('source-read', ultimateReplace)
    